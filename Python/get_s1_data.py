# Python pseudocode for downloading S1 data through the Sentinel API
# See https://sentinelsat.readthedocs.io/en/stable/api.html

# import libraries
from sentinelsat import SentinelAPI, read_geojson, geojson_to_wkt
from datetime import date

# authentication / connect
api = SentinelAPI('username', 'password', 'https://scihub.copernicus.eu/dhus')

# read region of interest
footprint = geojson_to_wkt(read_geojson('/path/to/roi.geojson'))

# query by polygon and time
products = api.query(footprint,
                     platformname='Sentinel-1',
                     producttype='SLC',
                     orbitdirection='DESCENDING',
                     beginposition='[YYYY-MM-DDT00:00:00.000Z TO YYYY-MM-DDT00:00:00.000Z]',
                     sensoroperationalmode='IW',
                     polarisationmode='VV')

# download
api.download_all(products, directory_path='/path/to/directory')

