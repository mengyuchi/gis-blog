# Table of Contents

* [Overview](./2_stamps_workflow.md)
* [Region of Interest](./2-1_roi.md)
* [Get SENTINEL-1 data](./2-2_get_data.md)
* [snap2stamps](./2-3_snap2stamps.md)
* [StaMPS](#4-stamps-ps-analysis)
* [Summary](#summary)
* [StaMPS Visualizer](./2-5_shiny.md)

# 4 StaMPS PS analysis
* StaMPS PS analysis is performed within MATLAB, using the main function `stamps()`,
  which takes the first and last StaMPS-step as main arguments. To run StaMPS
  complete with default arguments, use `stamps(1,8)`.
* However, while default values are reasonable, certain parameters do have potentially
  large impact on the quality of the obrained results. It is therefore stronlgy
  recommended to execute StaMPS step by step and explore the effects of adjusting
  certain parameters.
* Currently set values for all parameters can be displayed via `getparm()`.
  Parameters can be set using
  ```matlab
  setparm('parameter_name', value)
  ```
* Plotting of results and export to Google Earth (kml) as well as creation of
  geopackages for use in QGIS is described below.

## StaMPS default parameter settings

```matlab
getparm()
                       clap_alpha: 1
                        clap_beta: 0.3000
         clap_low_pass_wavelength: 800
                         clap_win: 32
                     density_rand: 20
                   drop_ifg_index: []
                 filter_grid_size: 50
                 filter_weighting: 'P-square'
         gamma_change_convergence: 0.0050
             gamma_max_iterations: 3
               gamma_stdev_reject: 0
                          heading: NaN
                  insar_processor: 'doris'
                           lambda: NaN
                    lonlat_offset: [0 0]
                     max_topo_err: 20
              merge_resample_size: 0
               merge_standard_dev: Inf
                          n_cores: 1
                     percent_rand: 20
                plot_color_scheme: 'inflation'
                 plot_dem_posting: 90
            plot_pixels_scatterer: 3
              plot_scatterer_size: 120
             quick_est_gamma_flag: 'y'
                ref_centre_lonlat: [0 0]
                          ref_lat: [-Inf Inf]
                          ref_lon: [-Inf Inf]
                       ref_radius: Inf
                     ref_velocity: 0
                      scla_deramp: 'n'
                  scla_drop_index: []
                      scla_method: 'L2'
                   scn_deramp_ifg: []
                 scn_kriging_flag: 'n'
                     scn_time_win: 365
                   scn_wavelength: 100
                    select_method: 'DENSITY'
          select_reest_gamma_flag: 'y'
                  shade_rel_angle: [90 45]
                          slc_osf: 1
              small_baseline_flag: 'n'
                      subtr_tropo: 'n'
                     tropo_method: 'a_l'
                     unwrap_alpha: 8
                unwrap_gold_alpha: 0.8000
                unwrap_gold_n_win: 32
                 unwrap_grid_size: 200
          unwrap_hold_good_values: 'n'
             unwrap_la_error_flag: 'y'
                    unwrap_method: '3D'
               unwrap_patch_phase: 'n'
            unwrap_prefilter_flag: 'y'
    unwrap_spatial_cost_func_flag: 'n'
                  unwrap_time_win: 730
                   weed_max_noise: Inf
                  weed_neighbours: 'n'
                weed_standard_dev: 1
                    weed_time_win: 730
              weed_zero_elevation: 'n'
```

## 4.1 Step 1: load PS candidates
Load the PS candidates selected via `mt_prep_gamma_snap`,
using amplitude dispersion as criterion. Data are converted into
the formats required for PS processing and stored in matlab workspaces.

```matlab
stamps(1,1)
```

Once step 1 has been run, details can be listed for each ifg by using

```matlab
ps_info
```

## 4.2 Step 2: calculate temporal coherence by estimating phase noise

```matlab
stamps(2,2)
```

To obtain teporal coherence, the spatially correlated phase is
estimated and subtracted. The spatially uncorrelated DEM error is
subsequently estimated and subtracted from the remaining phase, too.
Eventually, temporal coherence is estimated from the risidual phase.
This is an iterative step that estimates the phase noise value for each
candidate pixel in every interferogram.

Processing is controlled by the following parameters:

| Parameter        | Default  | Description       | level of influence      |
| ---------------- | -------- | ----------------- | ----------------------- |
| `max_topo_err`             | 20          | Maximum uncorrelated DEM error (in m). Pixels with uncorrelated DEM error greater than this will not be picked. This includes error due to the phase center of the resolution element being off-set from the middle of the pixel in range). Setting this higher, however, increases the mean $\gamma$ value (coherence-like measure) of pixels that have random phase. Typical values are around 10 m. <br> Raw interferograms contain a geometric phase term which is due to the master and slave images being acquired from different points in space. As in conventional InSAR, we correct for this geometric phase in two steps. First, we flatten the interferograms, which involves correcting the phase of each pixel as if the scattering surface were lying on a reference ellipsoid. Next, we estimate the phase due to the deviation of the real surface from the reference ellipsoid by transforming a DEM into the radar coordinate system. Two error terms arise in this processing, look angle error and squint angle error. | unknown |
| `filter_grid_size`         | 50          | Pixel size of grid (in m). Candidate pixels are resampled to a grid with this spacing before filtering to determine the spatially-correlated phase. <br> First, the noise term must be isolated in order to assess the phase stability. To do this, the first four terms on the right side of equation $`\Phi^w_{int,x,k} = W {\Phi_{def,x,i} + \Phi_{atm,x,i} + \Delta\Phi_{orb,x,i} + \Phi_{n,x,i}}`$ are estimated for each point and subtracted from the measured phase. The first three and parts of the fourth term are spatially correlated and can be estimated using adaptive bandpass filtering. This bandpass filtering is based on the combination of a simple low pass filter with an adaptive filter in the frequency domain, wherein the transfer function of the adaptive part of the filter is calculated on the basis of the smoothed data spectrum. The construction of this adaptive filter portion is based on the Goldstein phase filter. For the application of this filter technique, the irregular grid of the provisionally selected points must be transformed to a regular grid, whereby the grid width must be sufficiently small, so that over an element of this grid no strong variation in the phase takes place (typical values would be 40 to 100 m). The filter works like the Goldstein filter on individual sections of the image (windowed Fourier transformation).| low |
| `filter_weighting`         | 'P-square'  | Weighting scheme (PS probability squared), the other possibility being 'SNR'. Candidate pixels are weighted during resampling according to this scheme. P-squared was found to be better (Hooper et al., 2007). <br> The band-pass filter is implemented as an adaptive phase filter combined with a low-pass filter, applied in the frequency domain. Each pixel is first weighted. P-square is the recommended weight selection method. P-squared weights each pixel based on the probability that it is persistent. Empirically, it was found that that setting the amplitude to $`1/[P(x \in PS)]^2`$, where $`P(x \in PS)`$ is the probability that the pixel is a PS pixel, can give better results than weighting with the SNR, in the sense that more PS pixels are found.| high |
| `clap_win`                 | 32          | CLAP (Combined Low-pass and Adaptive Phase) filter window size (pixel x pixel). Depending on over what distance pixels are expected to remain spatially correlated. Together with filter grid size, determines the area included in the spatially-correlated phase estimation. <br> The 2-D FFT is applied to a grid size of typically 32 x 32 or 64 x 64 cells, depending on over what distance we expect pixels to remain spatially correlated. | unkown |
| `clap_low_pass_wavelength` | 800         | CLAP filter low-pass contribution cut-off spatial wavelength (in m). Wavelengths longer than this are passed. <br> This is the typical cutoff wavelength of Butterworth filter (see `clap_alpha`). Here, $`L(u,v)`$ denotes the transfer function of the fifth order Butterworth filter, which is characterized by a horizontal curve up to a cut-off frequency (this frequency typically corresponds to 800 m spatial wavelength) and then drops rapidly. | medium |
| `clap_alpha`               | 1           | Weighting parameter for phase filtering (CLAP $`\alpha`$ term). Together with the $`\beta`$ term, determines the relative contribution of the low-pass and adaptive phase elements to the CLAP filter. <br> The adaptive phase filter response, $`H(u,v)`$, is combined with a narrow low-pass filter response, $`L(u,v)`$, to form the new filter response, $`G(u,v) = L(u,v) + \beta (\frac{H(u,v)}{\widetilde{H}(u,v)}-1)^\alpha`$ where $`L(u,v)`$ is a fifth-order Butterworth filter, with a typical cutoff wavelength of 800 m, $`\alpha`$ and $`\beta`$ are adjustable weighting parameters (typical values being 1 and 0.3, respectively)and $`\widetilde{H}(u,v)`$ is the median value of $`H(u,v)`$.| unknown |
| `clap_beta`                | 0.3         | Weighting parameter for phase filtering (CLAP $`\beta`$ term). See `clap_alpha`. | unknown |
| `gamma_change_convergence` | 0.005       | Threshold for change in change in mean value of $`\gamma`$ (coherence-like measure). Determines when convergence is reached and iteration ceases. <br> $`\gamma`$ is a measure of the phase noise level and an indicator of whether the pixel is a PS pixel (i.e. a measure of the variation of residual phase for a pixel). Once we have converged on estimates for the phase stability of each pixel, we select those most likely to be PS pixels, with a threshold determined by the fraction of false positives we deem acceptable. We also seek to reject pixels that persist only in a subset of the interferograms and those that are dominated by scatterers in adjacent PS pixels. After every iteration we calculate the root-mean-square change in $`\gamma_x`$. When it ceases to decrease, the solution has converged and the algorithm stops iterating. | low |
| `gamma_max_iterations`     | 3           | Maximum number of iterations before iteration process ceases. <br> Phase Stability Estimation: Initially, we select a subset of pixels based on analysis of their amplitudes, rejecting those least likely to be PS pixels. We then estimate the phase stability of each of these pixels through phase analysis, which we successively refine in a series of iterations. | unknown |
| `quick_est_gamma_flag`     | 'y'         | Employ quick estimation procedure for $`\gamma`$ value. | unknown |

## 4.3 Step 3: PS selection

```matlab
stamps(3,3)
```

Pixels are selected on the basis of their noise characteristics. This step also
estimates the percentage of random (non-PS) pixels in a scene from which the
density per km² can be obtained.

Processing is controlled by the following parameters:

| Parameter        | Default  | Description       | level of influence      |
| ---------------- | -------- | ----------------- | ----------------------- |
| `select_method` | 'DENSITY' |	Selection method for pixels with random phase. Other option 'PERCENT'. <br> We select those pixels most likely to be PS pixels, with a threshold determined by the fraction of false positives we deem acceptable. We select PS based on the calculated values of $`\gamma_x`$ (measure of the phase stability of the pixel). Any pixel with random phase has a finite chance of having high $`\gamma_x`$ and therefore we can only select in a probablistic fashion. We therefore find a threshold value $`\gamma_{thresh}`$ that maximizes the number of real PS selected while keeping the fraction of random phase pixels selected (false positives) below a specified value. | unknown |
| `density_rand`  | 20        | Maximum acceptable spatial density (per km²) of selected pixels with random phase. Only applies if `select_method` is 'DENSITY'. At this stage we can usually accept a high density, as most random-phase pixels will be dropped in the next step. | unknown |
| `percent_rand`  | 20        | Maximum acceptable percentage of selected pixels having random phase. They are selected on the basis of their noise characteristics. The value can be set higher, since bad pixels will be weeded in the next steps (4 and 5). Only applies if `select_method` is 'PERCENT'.| medium |

## 4.4 Step 4: PS weeding

```matlab
stamps(4,4)
```

Pixels selected in the previous step are weeded, dropping those that are due to
signal contribution from neighbouring ground resolution elements and those
deemed too noisy. Data for the selected pixels are stored in new workspaces.

Processing is controlled by the following parameters:

| Parameter        | Default  | Description       | level of influence      |
| ---------------- | -------- | ----------------- | ----------------------- |
| `weed_standard_dev`   |	1 | Threshold standard deviation. For each pixel, the phase noise standard deviation for all pixel pairs including the pixel is calculated. If the minimum standard deviation is greater than the threshold, the pixel is dropped. If set to 10, no noise-based weeding is performed. <br> A pixel may display a period of good phase stability, preceded and/or followed by a period of low stability. To avoid picking these pixels, we include an optional step to estimate the variance of x for each pixel using the bootstrap percentile method with 1000 iterations, and drop pixels with standard deviation over a defined threshold, a typical value being 1. | high: <br> increasing the SD increases the selected PS but also the noise level.[1-1.2] are good initial values |
| `weed_max_noise`      | Inf | Threshold for the maximum noise allowed for a pixel. For each pixel the minimum pixel-pair noise is estimated per interferogram. Pixels whose maximum interferogram noise value is higher than the indicated threshold are dropped. <br> Finally, pixels can be sorted out based on their noise. Since the influence of the atmosphere varies greatly from shot to shot and the phases are wound up, absolute phase values for each pixel are decorrelated over time. If, however, differences are formed between adjacent PS pixels, the neighborhood being determined by a Delaunay triangulation, the phase ambiguity is emphasized. A time series of phase differences is now available for each edge, from which the standard deviation of the noise of the difference phase can be derived, which is achieved by subtracting a lowpass component and calculating the standard deviation. A pixel is then removed from the set of PS if the edge with the smallest standard deviation incidente to this pixel exceeds a set threshold value. | unknown |
| `weed_time_win`       | 730 | Smoothing window (in days) for estimating phase noise distribution for each pair of neighbouring pixels. The time series phase for each pair is smoothed using a Gaussian-weighted piecewise linear fit. weed time win specifies the standard deviation of the Gaussian. The original phase minus the smoothed phase is assumed to be noise. | unknown |
| `weed_neighbours`     | 'n' | Flag for proximity weeding. If set to 'y', pixels are dropped based in their proximity to each other. <br> A Scatterer that is bright can dominate pixels other than the pixel corresponding to its physical location. The error in look angle and squint angle due to the offset of the pixel from the physical location usually results in these pixels not being selected as PS. However, the slight oversampling of the resolution cells can cause pixels immediately adjacent to the PS pixel to be dominated by the same scatterer where the error may be sufficiently small that the pixel appears stable. To avoid picking these pixels, we assume that adjacent pixels selected as PS are due to the same dominant scatterer. As we expect the pixel that corresponds to the physical location to have the highest SNR, for groups of adjacent stable pixels we select as the PS only the pixel with the highest value of $`\gamma_x`$. | high: <br> should be set to 'y' |
| `weed_zero_elevation` | 'n' | unknown | unknown |

## 4.5 Step 5: Phase correction

```matlab
stamps(5,5)
```

The wrapped phase of the selected pixels is corrected for spatially-uncorrelated
look angle (DEM) error. At the end of this step the patches are merged.

Processing is controlled by the following parameters:

| Parameter        | Default  | Description       | level of influence      |
| ---------------- | -------- | ----------------- | ----------------------- |
| `merge_resample_size` |  0  | Coarser posting (in m) to resample to. If set to 0, no resampling is applied. Use in case of memory issues to apply coarser sampling. | unknown |
| `merge_standard_dev`  | Inf | Threshold standard deviation. For each resampled pixel, the phase noise standard deviation is computed. If the standard deviation is greater than the threshold, the resampled pixel is dropped. Only applied in case of resampling. | unknown |


Plotting:
* `ps_plot('w')`: Check the wrapped phase of the selected pixels. In terms of
  reprocessing, the first parameter to play with is `weed_standard_dev`.
  If it looks like too many noisy pixels are being chosen,
  the value can be reduced. If very few pixels are chosen, the value can
  be increased. If still too few pixels are being selected such that any
  signal is generally undersampled, variation of Step 2 parameters can be
  tried. The number of initial candidates can also be increased by setting
  the amplitude dispersion higher in `mt_prep`.

*Note*: If a combination of PS and SBAS is intended, data for both have to
processed to this step before continue with step 6.

## 4.6 Step 6: Phase unwrapping
The $`2\pm\pi`$ discontinuity of the extracted phase appears when an extreme value
($`-\pi / +\pi`$) is reached. The phase then jumps to the other end of the interval,
even though physically the optical phase is continuous and relatively softly
increasing or decreasing. 

```matlab
stamps(6,6)
```
Processing is controlled by the following parameters:

| Parameter        | Default  | Description       | level of influence      |
| ---------------- | -------- | ----------------- | ----------------------- |
| `unwrap_method`           | '3D' | Unwrapping method, alternative '3D_QUICK' for SBAS. '3D_QUICK' is default for SBAS. Can be set to '3D' to potentially improve accuracy, although this will take longer. <br> PS data sets are in fact three-dimensional, the third dimension being that of time, and unwrapping accuracy is improved by treating the problem as 3-D. | medium/high |
| `unwrap_grid_size`        | 200  | Resampling grid spacing. If `unwrap_prefilter_flag` is set to 'y', phase is resampled to this grid. Should be at least as large as `merge_grid_size`. | high: <br> high values reduces noise but may lead to undersampling of the signal |
| `unwrap_gold_alpha`       | 0.8  | Value of $`\alpha`$ for Goldstein filter. The larger the value the stronger the filtering. | medium/high |
| `unwrap_gold_n_win`       | 32   | Window size for Goldstein filter. <br> Optionally, if the data are very noisy, a pre-filtering step can be run to filter the data spatially before unwrapping, as is common in 2-D ([Goldstein and Werner, 1998](https://doi.org/10.1029/1998GL900033)). For each time step, the complex phase data are first sampled to a grid using a grid spacing over which little variation in phase is expected (typically 40 to 100 m). Where multiple pixels fall in the same grid cell, the complex phase is summed. The gridded phase is then transformed using the fast Fourier transform and filtered in the frequency domain using an adaptive phase filter [Goldstein and Werner, 1998]. This preserves the dominant frequencies which are present in the data. After inverse transformation, the grid cells containing data are treated as the new data points for input into the unwrapping algorithm. | unknown |
| `unwrap_hold_good_values` | 'n'  | - | - |
| `unwrap_prefilter_flag`   | 'y'  | Prefilter phase before unwrapping to reduce noise. SCLA and AOE subtraction is applied when step 6 is redone after step 7. To avoid subtraction use `scla_reset` | high: <br> setting to 'y' is recommended |
| `unwrap_patch_phase`      | 'n'  | Use the patch phase from Step 3 as prefiltered phase. If set to 'n' PS phase is filtered using a Goldstein adaptive phase filter. | high: <br> setting to `'n'` is recommended |
| `unwrap_time_win`         | 730  | Smoothing window (in days) for estimating phase noise distribution for each pair of neighbouring pixels (i.e. smoothing filter length in days for smoothing phase in time by estimate the noise contribution for each phase arc). The time series phase for each pair is smoothed using a Gaussian window with standard deviation of this size. Original phase minus smoothed phase is assumed to be noise, which is used for determining probability of a phase jump between the pair in each interferogram. <br> With a Gaussian filter, the low frequencies of the time series are determined and subtracted, which leads to the noise noise measurements whose variance is estimated.| high |
| `drop_ifg_index`          | []   | ifg will not be included in unwrapping. This might be helpful if after unwrapping and testing parameters an ifg is still noisy. After omitting interferograms, steps from 2 onwards should be redone. | potentially high (if needed) |

*Note* In case of re-running Step 6 after a full StaMPS run, estimates of SCLA
and master atmosphere and orbit error (AOE) will be subtracted before
unwrapping. If you do not wish this to occur, reset these estimates before
running Step 6 again with `scla_reset`. This is useful if one is interested
in local signals only, or when looking for landslides in a larger area.
This subtraction of SCLA and master AOE has not been implemented with the
`unwrap_prefilter_flag = 'n'` option.)

Plotting:
* `ps_plot('u')`: Check for unwrapping errors i.e., phase jumps in space which are uncorrelated in time.

## 4.7 Step 7: Estimate spatially-correlated look angle error

```matlab
stamps(7,7)
```

Processing is controlled by the following parameters:

| Parameter        | Default  | Description       | level of influence      |
| ---------------- | -------- | ----------------- | ----------------------- |
| `scla_deramp`    | 'n'      | If set to 'y', a phase ramp is estimated for each interferogram. The estimated ramp will be subtracted before unwrapping. This is useful if one is interested in local signals only. | medium/unknown <br> (interesting when looking for landslides in a larger area) |
| `drop_ifg_index` | []       | Specified ifg will not be included in unwrapping. This might be helpful if after unwrapping and testing parameters an ifg is still noisy. Than the index can be set and steps from 2 should be redone. | high if needed |

Plotting:
* `ps_plot('d')`: estimate of SCLA error. Units are phase per m of perpendicular
  baseline, with 0.01 radians/m corresponding to about 12 m of DEM error for
  the Envisat I2 swath.
* `ps_plot('m')`: estimate of master atmosphere and orbit error (AOE) phase
* `ps_plot('o')`: phase ramps (if `scla_deramp` is set to 'y') 

Unwrapped phase minus one of, or a combination of the above can be plotted with
`u-d`, `u-m`, `u-o`, `u-dm`, `u-do`, or `u-dmo`’.

After running step 7, check that the estimates seem reasonable, i.e.
`ps plot('u-dm')` looks generally smoother than `ps plot('u')`
(note that the default colour scales will be different). If not generally
smoother, one or more interferograms have probably been incorrectly unwrapped
(usually those with large perpendicular baselines). 

## 4.8 Step 8: Atmospheric filtering

```matlab
stamps(8,8)
```

StaMPS step 8 filters the data in a way to address noise coming from atmospheric
disturbances. It is steered by 2 parameters: `scn_wavelength` and `scn_time_win`.

| Parameter        | Default  | Description        | level of influence      |
| ---------------- | -------- | ------------------ | ----------------------- |
| `scn_wavelength` | 100      | Spatial filter     | high                    |
| `scn_time_win`   | 365      | Temporal filter (Low-pass filter over time): Finally, after the absolute values have been derived, the signal is subjected to low-pass filtering, which eliminates the existing noise term, resulting in an estimated value for $`\Phi_^s_{atm,x,i} + \Delta\Phi^s_{orb,x,i}`$. | high |
| `subtr_tropo`    | 'n'      | When set to 'y', removes an estimate before unwrapping as defined using the correction in `tropo_method`. Note that this is to ease the unwrapping process, thus after unwrapping the tropospheric signals are added back in. | - |
| `tropo_method`   | 'a_l'    | Type of tropospheric correction that will be used to remove the tropospheric delays when subtr_tropo is set to 'y'. Valid options are those supported by TRAIN (e.g. 'a_linear', 'a_gacos', 'a_erai', etc). | - |

It is however warmly recommended to adjust those parameters according to your
dataset. Play around with these parameters in order to achieve the desired result.

Plotting:
* `ps_plot('v-do')`: displacement velocity in mm/year, with positive values being towards the satellite.
  `v-do` indicates that DEM error and oribital ramps are subtracted before estimating mean velocity.

```matlab
% save PS velocity estimation to a mat file
>> ps_plot('v-dao', -1)
% load matfile
>> load ps_plot_v-dao
% save ps.kml (generated from ph_disp for every 10 points with an opacity of 0.4
>> ps_gescatter('ps.kml', ph_disp, 10, 0.4)
```

## 4.9 Step 9: TRAIN

**Notes**

- edit `aps_weather_model_times.m` to avoid the need for the Financial Toolbox.
  Simply replace `today` by `floor(now)` in line 19.

- edit `aps_weather_model_InSAR.m` since `setparm_aps('lambda', ...)` does not
  seem to be working correctly. Set `lambda = 0.0547` (wavelength [m] for S1)
  in line 74.

- Conclusions from Hooper et al. (2015): [Statistical comparison of InSAR tropospheric correction techniques](https://doi.org/10.1016/j.rse.2015.08.035).
  Remote Sensing of Environment, 170 40-47:
  *Our analysis included methods based on spectrometer measurements, output of
  weather models, and empirical interferometric phase-based methods. When
  available and limited to cloud-free and daylight acquisitions only, we found the
  spectrometers to provide the largest RMSE reduction. We found that the estimated
  tropospheric delays using MODIS have at best an accuracy equal to that of MERIS,
  and at worst twice that of MERIS. We found the phase-based methods (linear and
  power-law) to outperform the weather model methods in regions where tropospheric
  delays are mainly correlated with topography. For regions over which this is
  less apparent, due to turbulence in the troposphere and dynamic local weather,
  weather models can potentially offer better performance. In those instances
  where weather models mis-estimate the location of turbulent features, they will
  have a correspondingly higher RMSE. We did not find a significant improvement
  when using a local high-resolution weather model (7 km and 2 km) instead of the
  global reanalysis products. With a longer required runtime, local weather model
  are less suitable for near real-time InSAR application. From a cloud cover
  analysis, we found the performance of the different correction methods to worsen
  with increasing cloud cover, with a ~ 10–20% increase in RMSE for each cloudy
  SAR date.*

### GACOS

```matlab
setparm_aps('gacos_datapath', ''...'')
setparm_aps('UTC_sat', 'HH:MM')  % use hh:mm from Sentinel-1 filename
aps_weather_model('gacos', 0, 0)
```

This yields a list of dates, ROI and time, which can be used to download
the required [GACOS](http://ceg-research.ncl.ac.uk/v2/gacos/) files manually.
Once the files have been downloaded and put into the `gacos_datapath` directory,
you can run

```matlab
aps_weather_model('gacos', 3, 3)
ps_plot('v-dao', 'a_gacos', 'ts')
```

### ERA-I

```matlab
setparm_aps('era_datapath’, '...')
setparm_aps('UTC_sat', 'HH:MM') % use hh:mm from Sentinel-1 filename
aps_weather_model('era', 0, 0)	% get ERA_I_files.txt
```

Download ERA-I in terminal:
```bash
get_ecmwf password user ERA_I_files.txt
```

Obtain a DEM file (e.g. STRM DEM, ASTER DEM, [oe3d](http://www.oe3d.at/download/))
and convert GeoTIFF to grd-file using `gdal_translate`
(or Raster > Conversion > Translate > Select > ASCII gridded XYZ > _.grd in QGIS)

```
setparm_aps('demfile','_.grd’)
setparm_aps('era_data_type', 'BADC')	
aps_weather_model('era', 2, 2)	
aps_weather_model('era', 3, 3)

ps_plot('v-dao', 'a_erai', 'ts')
```

# Summary
Overview of the most important parameters that might require adjustment, including
sensible new values for PS processing:

| Parameter           | Default    | Proposed  | Description   |
| ------------------- | ---------- | --------- | ------------- |
| `scla_deramp`       |       'n'  |       'y' | If set to 'y', a phase ramp is estimated for each interferogram. The estimated ramp will be subtracted before unwrapping. This is useful if one is interested in local signals only (interesting when looking for landslides in a larger area). |
| `unwrap_gold_n_win` |        32  |         8 | Window size for Goldstein filter. |
| `unwrap_grid_size`  |       200  |        10 | Resampling grid spacing. If `unwrap_prefilter_flag` is set to 'y', phase is resampled to this grid. Should be at least as large as `merge_grid_size`. | 
| `unwrap_time_win`   |       730  |        24 | Smoothing window (in days) for estimating phase noise distribution for each pair of neighbouring pixels (i.e. smoothing filter length in days for smoothing phase in time by estimate the noise contribution for each phase arc). The time series phase for each pair is smoothed using a Gaussian window with standard deviation of this size. Original phase minus smoothed phase is assumed to be noise, which is used for determining probability of a phase jump between the pair in each interferogram. Could be importand when looking for Landslides. See `unwrap_grid_size` |
| `scn_time_win`      |       365  |        50 | Temporal filtering window size. |
| `ref_centre_lonlat` |     [0 0]  | [lon lat] | Set circular reference area (circle centre) |
| `ref_radius`        |       Inf  |        40 | Set circular reference area (circle radius) |
| `ref_lat`           | [-Inf Inf] | [min max] | Set rectangular reference area (lat range) |
| `ref_lon`           | [-Inf Inf] | [min max] | Set rectangular reference area (lon range) |

Ad reference area: If no reference area is set, the reference value is the mean value for the whole area.

### Volcano
(Volcán Alcedo, based on [Hooper 2007](https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2006JB004763))

| Parameter          | Default  | Used        |
| ------------------ | -------- | ----------- |
| `max_topo_err`     |  20      |  10         |
| `filter_grid_size` |  50      |  40         |
| `clap_win`         |  32      |  64         |
| `percent_rand`     |  20      |   1         |
| `unwrap_grid_size` | 200      | 100         |
| `unwrap_time_win`  | 730      | 180         |
| `scn_time_win`     | 365      | 180         |
| `scn_wavelength`   | 100      |  50         |

### Landslide
(Doubrava, based on [Lazecký 2011](http://www.insar.cz/diplomky/diz_lazecky.pdf))

| Parameter           | Default  | Used        |
| ------------------- | -------- | ----------- |
| `max_topo_err`      |  20      |  10         |
| `clap_win`          |  32      |  16         |
| `weed_standard_dev` |   1      |   1.5       |
| `unwrap_grid_size`  | 200      | 100         |
| `unwrap_time_win`   | 730      | 365         |
| `unwrap_gold_alpha` | 0.8      |   5         |

### Landslide
(based on [Höser 2018](https://doi.org/10.13140/RG.2.2.35085.59362))

| Parameter          | Default  | Used        |
| ------------------ | -------- | ----------- |
| `scla_deramp`       |  'n'     |  'y'        |
| `unwrap_time_win`   | 730      |  24         |
| `unwrap_grid_size`  | 200      |  10         |
| `unwrap_gold_n_win` |  32      |   8         |
| `scn_time_win`      | 365      |  50         |

### Bridge
(extremely educated guesses by BW and MS)

| Parameter           | Default  | Used        |
| ------------------- | -------- | ----------- |
| `max_topo_err`      |  20      |  10         |
| `filter_grid_size`  |  50      |  40         |
| `clap_win`          |  32      |  16         |
| `scla_deramp`       |  'n'     |  'y'        |
| `percent_rand`      |  20      |   1         |
| `unwrap_grid_size`  | 200      |  50         |
| `unwrap_time_win`   | 730      | 180         |
| `scn_time_win`      | 365      | 180         |
| `scn_wavelength`    | 100      |  50         |
| `unwrap_gold_n_win` |  32      |  16         |
